const Tour = require("../models/Tour");

exports.createTour = async (req, res) => {
  try {
    const tour = await Tour.create(req.body);
    res.status(200).json({
      status: "success",
      data: {
        tour
      }
    });
  } catch (error) {
    res.status(400).json({
      status: "failed!",
      message: error.message
    });
  }
};
exports.getAllTours = async (req, res, next) => {
  try {
    let queryObj = { ...req.query };

    // 1A) FILTER
    const excludedFields = ["sort", "limit", "page", "fields"];

    excludedFields.forEach(el => delete queryObj[el]);

    // 1B) ADVANCED FILTER ...
    let queryString = JSON.stringify(queryObj);

    queryString = queryString.replace(
      /\b(gte|gt|lte|lt)\b/g,
      match => `$${match}`
    );

    let query = Tour.find(JSON.parse(queryString));

    // 2) SORTING ...
    if (req.query.sort) {
      const sortBy = req.query.sort.split(",").join(" ");
      query = query.sort(sortBy);
    } else {
      query = query.sort("-createdAt");
    }

    // 3) SELECTING SPECIFIC FIELDS ONLY ...
    if (req.query.fields) {
      const fields = req.query.fields.split(",").join(" ");
      query = query.select(fields);
    } else {
      query = query.select("-__v");
    }

    // 4)PAGINATION ...
    const page = req.query.page * 1 || 1;
    const limit = req.query.limit * 1 || 100;
    const skip = (page - 1) * limit;

    query = query.skip(skip).limit(limit);
    if (req.query.page) {
      const totalTours = await Tour.countDocuments();
      if (skip >= totalTours) {
        throw new Error("This page is not exits!");
      }
    }
    const tours = await query;

    res.status(200).json({
      status: "success",
      results: tours.length,
      data: {
        tours
      }
    });
  } catch (error) {
    res.status(404).json({
      status: "failed",
      message: error.message
    });
  }
};
exports.getTour = async (req, res, next) => {
  try {
    const tour = await Tour.findById(req.params.id);
    res.status(200).json({
      status: "success",
      data: {
        tour
      }
    });
  } catch (error) {
    res.status(404).json({
      status: "failed",
      message: error.message
    });
  }
};

exports.updateTour = async (req, res, next) => {
  try {
    const tour = await Tour.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });
    res.status(200).json({
      status: "success",
      data: {
        tour
      }
    });
  } catch (error) {
    res.status(404).json({
      status: "failed",
      message: error.message
    });
  }
};
exports.deleteTour = async (req, res, next) => {
  try {
    await Tour.findByIdAndDelete(req.params.id);
    res.status(200).json({
      status: "success",
      data: null
    });
  } catch (error) {
    res.status(404).json({
      status: "failed",
      message: error.message
    });
  }
};
