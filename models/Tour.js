const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TourSchema = new Schema({
  name: {
    type: String,
    required: [true, "Tour must have a name "],
    unique: true
  },
  duration: {
    type: Number,
    required: [true, "Tour must have a duration"]
  },
  maxGroupSize: {
    type: Number,
    required: [true, "Tour must have a maxGroupSize"]
  },
  difficulty: {
    type: String,
    required: [true, "Tour must have a difficulty level"]
  },
  ratingsAverage: {
    type: Number,
    default: 4.5
  },
  ratingsQuantity: {
    type: Number,
    default: 0
  },
  price: {
    type: Number,
    required: [true, "Tour must have a price"]
  },
  priceDiscount: Number,
  summary: {
    type: String,
    required: [true, "Tour must have a summary"],
    trim: true
  },
  description: {
    type: String,
    trim: true
  },
  imageCover: {
    type: String,
    required: [true, "Tour must have a imageCover"]
  },
  images: [String],

  createdAt: {
    type: Date,
    default: Date.now(),
    select: false
  },
  startDates: [Date]
});
module.exports = mongoose.model("tours", TourSchema);
