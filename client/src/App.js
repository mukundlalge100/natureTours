import React, { Component, Suspense, lazy } from "react";
import { Route, Redirect, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PageNotFound from "./Components/PageNotFound/PageNotFound";
import Demo from "./Demo";
import Loader from "./Components/UI/Loader/Loader";
// import * as actions from "./Store/Actions/IndexAction";

//STYLLING IMPORTS
import classes from "./App.module.scss";

// ASYNC LAZY COMPONENTS CHUNKS ...

class App extends Component {
  componentDidMount = () => {
    // CHECK IF TOKEN FOR USER IS SET OR NOT IN LOCALSTORAGE IF YES THEN LOGIN USER EVEN AFTER RELOADING...
    // this.props.onAuthTryToLogIn(this.props.history);
  };

  render() {
    let routes;

    if (this.props.isAuthenticated) {
      routes = (
        // PRIVATE ROUTES ...
        <Switch>
          <Route exact path="/pagenotfound" component={PageNotFound} />
          <Redirect exact to="/" />
        </Switch>
      );
    } else {
      routes = (
        // PUBLIC ROUTES ...
        <Switch>
          <Route exact to="/" component={Demo} />
        </Switch>
      );
    }
    return (
      <main className={classes.App}>
        <Suspense
          fallback={
            <div className={classes.App_Loader}>
              <Loader />
            </div>
          }
        >
          {routes}
        </Suspense>
      </main>
    );
  }
}
const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated
  };
};
const mapDispatchToProps = dispatch => {
  return {
    // onAuthTryToLogIn: history => dispatch(actions.authCheckLogInState(history))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(App));
