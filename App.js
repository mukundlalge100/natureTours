const express = require("express");
const app = express();
const fs = require("fs");
const morgan = require("morgan");
const rfs = require("rotating-file-stream");
const path = require("path");
const userRoutes = require("./routes/UserRoutes");
const tourRoutes = require("./routes/TourRoutes");

const mongoose = require("mongoose");

const dotenv = require("dotenv");

dotenv.config({ path: "./config/config.env" });

// BODY PARSER MIDDLEWARE WORKS ABOVE 4.16 VERSION IN EXPRESS...
app.use(express.json());

const port = process.env.PORT || 5000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// DB CONNECTION ...
mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
  .then(() => console.log("MongoDB Connected!"))
  .catch(error => console.error(error));

// MORGAN MIDDLEWARE AND RFS CONFIGATION SETTING...
const logDirectory = path.join(__dirname, "logs");
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
const accessLogStream = rfs.default("access.log", {
  interval: "1d",
  path: logDirectory
});
app.use(morgan("combined", { stream: accessLogStream }));

// // CORS SETTING ...
// app.use((request, response, next) => {
//   response.setHeader("Access-Control-Allow-Origin", "*");
//   response.setHeader(
//     "Access-Control-Allow-Methods",
//     "OPTIONS,PUT,POST,GET,PATCH,DELETE"
//   );
//   response.setHeader(
//     "Access-Control-Allow-Headers",
//     "Content-Type,Authorization"
//   );
//   next();
// });

// SETTING ROUTES ...
app.use("/api/v1/tours/", tourRoutes);
app.use("/api/v1/users", userRoutes);

// SERVER STATIC ASSETS IF IN PRODUCTION ...
if (process.env.NODE_ENV === "production") {
  // SET STATIC FOLDER ...
  app.use(express.static("client/build"));

  app.get("*", (request, response) => {
    response.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}
